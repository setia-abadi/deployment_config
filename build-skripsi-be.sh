#!/bin/bash
set -e

DOCKER_FOLDER_PATH=/root/deployment
CAPSTONE_FOLDER_PATH=/root/deployment/skripsi-be

# update local repository
cd $CAPSTONE_FOLDER_PATH
git stash -u && git stash clear && git pull --rebase origin "${1:-master}"

# build Dockerfile and Dockerfile.tools and Dockerfile.scheduler
cd $CAPSTONE_FOLDER_PATH
docker build -t skripsi-be-full -f Dockerfile.full .
# docker build -t skripsi-be -f Dockerfile .
# docker build -t skripsi-be-tools -f Dockerfile.tools .
# docker build -t skripsi-be-scheduler -f Dockerfile.scheduler .

# docker-compose deploy
cd $DOCKER_FOLDER_PATH
docker-compose up -d skripsi-be
docker-compose up -d skripsi-be-tools
docker-compose up -d skripsi-be-scheduler

# update docs
docker-compose exec go make generate-docs

docker-compose exec go make sync-permission

# prune docker image
# docker image prune -f
#docker images -a | grep none | awk '{ print $3; }' | xargs docker rmi --force